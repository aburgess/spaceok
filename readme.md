# SpaceOK

## Overview

I'm just having fun making a game in my spare time.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Features](#features)
- [Contributing](#contributing)
- [FAQ](#faq)
- [Community Support](#community-support)
- [License](#license)
- [Acknowledgements](#acknowledgements)
- [License](#license)

## Installation

Provide instructions on how to install and set up your project. Include any dependencies and steps needed to run the project.

```bash
# Example installation command or steps
git clone https://gitlab.ucc.asn.au/aburgess/spaceok.git
cd spaceok
make run
```

## Usage

### Controls

### Config

## Features

### Core Gameplay

1. **Turn-Based Gameplay:**
   - Implement a turn-based system where players take turns to make decisions and actions.

2. **Hexagon Grid World:**
   - Design and render the game world using a hexagon grid system.

3. **Procedural World Generation:**
   - Generate the game world dynamically with procedural algorithms.

4. **Resource Management:**
   - Include a system for collecting, managing, and utilizing in-game resources.

5. **Player and NPC Interaction:**
   - Implement interactions between the player and non-player characters (NPCs).

### Game Mechanics

6. **Combat System:**
   - Develop a combat system with attacks, defenses, and damage calculations.

7. **Quest and Mission System:**
   - Create a system for assigning quests or missions to players.

8. **Character Progression:**
   - Design a system for character development and progression, including leveling up and skill acquisition.

9. **Inventory System:**
   - Implement an inventory system for storing and managing items.

10. **Day-Night Cycle:**
    - Include a dynamic day-night cycle that affects gameplay and visuals.

## User Interface (UI)

11. **Main Menu:**
    - Design an intuitive main menu for starting, loading, and exiting the game.

12. **In-Game HUD:**
    - Create a heads-up display (HUD) to show essential information during gameplay.

13. **Dialog System:**
    - Implement a dialogue system for in-game conversations.

14. **Map and Navigation:**
    - Provide a map and navigation system to help players navigate the game world.

### Graphics and Sound

15. **2D Art and Animation:**
    - Integrate 2D art and animations for characters, objects, and visual effects.

16. **Sound Effects and Music:**
    - Include sound effects and background music to enhance the gaming experience.

### Additional Features

17. **Save and Load System:**
    - Develop a system for saving and loading game progress.

18. **Achievements and Rewards:**
    - Implement achievements and reward systems for completing specific tasks or milestones.

19. **Multiplayer Functionality:**
    - Optionally, add multiplayer features such as co-op or competitive gameplay.

20. **Modding Support:**
    - Provide support for modding, allowing players to create and share custom content.

### Future Enhancements

21. **Additional Game Modes:**
    - Consider future game modes or expansions to extend the replay value.

22. **Accessibility Features:**
    - Explore options for making the game accessible to a broader audience.

23. **Cross-Platform Compatibility:**
    - Investigate the possibility of making the game compatible with multiple platforms.

24. **Community and Social Integration:**
    - Integrate features that encourage community interaction and social sharing.

## Contributing

If you would like others to contribute to your project, provide guidelines on how to do so.

1. Fork the project
2. Create a new branch (`git checkout -b feature/new-feature`)
3. Commit your changes (`git commit -am 'Add new feature'`)
4. Push to the branch (`git push origin feature/new-feature`)
5. Open a pull request

## FAQ

## Community Support

## License

## Acknowledgements
