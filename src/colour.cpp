#include "colour.h"

Colour::Colour(  )
	: _col( {0x00, 0x00, 0x00, 0x00} )
{}
Colour::Colour( uint8_t r, uint8_t g, uint8_t b, uint8_t a )
	: _col( {r,g,b,a} )
{}
Colour::~Colour()
{}

RGBA Colour::getRGBA() { return this->_col; }
void Colour::brighten( uint8_t delta )
{
	if( this->_col.r < 0xFF ) this->_col.r += delta;
	if( this->_col.g < 0xFF ) this->_col.g += delta;
	if( this->_col.b < 0xFF ) this->_col.b += delta;
}
void Colour::darken( uint8_t delta )
{
	if( this->_col.r > 0 ) this->_col.r -= delta;
	if( this->_col.g > 0 ) this->_col.g -= delta;
	if( this->_col.b > 0 ) this->_col.b -= delta;
}