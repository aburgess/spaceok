#include "config-parser.h"

ConfigParser ConfigParser::UserSettings()
{
	ConfigParser parser("settings.cfg");
	return parser;
}

void ConfigParser::set( std::string key, std::string value )
	{ this->_config[key] = value; }

std::string ConfigParser::get( std::string key )
	{ return this->_config[key]; }

std::string ConfigParser::toString() const
{
	std::string str = "Config File: "+this->_path+"\n";
	for( const auto& [key, value]  : this->_config )
		{ str += "["+key+"] "+value+"\n"; }
	return str;
}


ConfigParser::ConfigParser( std::string path )
	: _path(path), _logger( Logger::instance() )
{
	this->_logger.info( "Creating config parser" );
	this->readFile( path );
}

ConfigParser::~ConfigParser()
	{ this->_logger.info( "Closing config parser" ); }


ConfigMap ConfigParser::readFile( std::string path )
{
	ConfigMap confMap;
	if( ! std::filesystem::is_directory( CONFIG_DIRECTORY ) )
	{
		this->_logger.warning( "Unable to read config directory" );
		return confMap;
	}

	std::ifstream file( CONFIG_DIRECTORY+"/"+path );
	if( file.is_open() )
	{
		std::string line;
		while( std::getline(file, line) )
		{
			if( ! line.empty() )
			{
				if( line.at(0) == COMMENT_CHAR )
					{ continue; } 
				std::string key = this->getKey( line );
				std::string value = this->getKey( line );
				if( ! key.empty() && ! value.empty() )
					{ confMap[key] = value; }
				std::cout << key << "," << value << std::endl;
					// { confMap.insert( keyPair(key, value) ); }
			}
		}
	} else 
		{ this->_logger.error( "Unable to load specified configuration file:\n"+path ); }

	this->_config = confMap;
	return confMap;
}

std::string ConfigParser::getKey( std::string line )
{
	std::string key = "";
	for( char ch : line )
	{
		if( ch == DELIMINATOR
				|| ch == COMMENT_CHAR
		) { break; }
		else if ( ! std::isspace(ch) ) { key.push_back( ch ) ; }
	}
	return key;
}

std::string ConfigParser::getValue( std::string line )
{
	std::string value = "";
	bool hasFound = false;
	for( char ch : line )
	{
		if( ch == DELIMINATOR
			|| COMMENT_CHAR == ch
		) { break; }
		else if ( hasFound ) { value.push_back( ch ) ; }
	}
	return value;
}

std::ostream& operator<<( std::ostream& os, const ConfigParser& obj )
{
	return os << obj.toString();
}
