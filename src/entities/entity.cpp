#include "entities/entity.h"

Entity::Entity(
		const std::string& tag, size_t id
) : _logger( Logger::instance() ),
	_id(id), _tag( tag )
{ this->_logger.info( "Generic Entity Created" ); }
Entity::~Entity(){}
