#include "logger.h"

Logger& Logger::instance()
{
	static Logger instance;
	return instance;
}
void Logger::setLogLevel( LogLevel level )
	{ currentLogLevel = level; } 
		
void Logger::debug( const std::string& message ) { this->log( LogLevel::DEBUG, message ); }
void Logger::info( const std::string& message ) { this->log( LogLevel::INFO, message ); }
void Logger::warning( const std::string& message ) { this->log( LogLevel::WARNING, message ); }
void Logger::error( const std::string& message ) { this->log( LogLevel::ERR, message ); }
void Logger::fatal( const std::string& message ) { this->log( LogLevel::FATAL, message ); }

Logger::Logger()
	: currentLogLevel( LogLevel::INFO )
{ this->info("New Logger instance created"); }
Logger::~Logger(){ this->info( "Closing Logger System" ); }

void Logger::log(LogLevel level, const std::string& message)
{
	#ifdef __COLOUR_CONSOLE
	std::string levelColour = this->getLevelColour(level);
	if( level >= currentLogLevel )
	{
		std::cout << "["<<BLK<<WHTB<<this->getCurrentTimestamp()<<CRESET<<"] "
			<< levelColour << message << CRESET << std::endl;
	}
	#else // __COLOUR_CONSOLE
	if( level >= currentLogLevel )
		{ std::cout << "[" << this->getCurrentTimestamp() << "] " << message << std::endl; }
	#endif // __COLOUR_CONSOLE
}

std::string Logger::logLevelToString( LogLevel level )
{
	switch(level)
	{
		case LogLevel::DEBUG: return "DEBUG";
		case LogLevel::INFO: return "INFO";
		case LogLevel::WARNING: return "WARNING";
		case LogLevel::ERR: return "ERROR";
		case LogLevel::FATAL: return "FATAL";
		default: return "UNKNOWN";
	}
}
std::string Logger::getLevelColour( LogLevel level )
{
	switch(level)
	{
		case LogLevel::DEBUG: return BLK;
		case LogLevel::INFO: return CYN;
		case LogLevel::WARNING: return YEL;
		case LogLevel::ERR: return RED;
		case LogLevel::FATAL: return MAG;
		default: return CRESET;
	}
}
std::string Logger::getCurrentTimestamp()
{
	auto now = std::chrono::system_clock::now();
	auto timestamp = std::chrono::system_clock::to_time_t(now);
	std::stringstream ss;
	ss << std::put_time( std::localtime(&timestamp), "%Y-%m-%d %H:%M:%S" );
	return ss.str();
}
