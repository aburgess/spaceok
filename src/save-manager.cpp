#include "save-manager.h"

SaveItem::SaveItem( std::string path )
	: _path( path )
{
	std::string name = "";
	for( char c : path )
	{
		if( c == '.' ) { break; }
		name += c;
	}

	if( name.length() )
			{ name[0] ^= 0b0100000; }
	this->_name = name;
}
SaveItem::~SaveItem() {}
std::string SaveItem::getString() const { return this->_name; }
std::ostream& operator<<( std::ostream& os, const SaveItem& obj )
{
	return os << obj.getString();
}


SaveManager::SaveManager()
	: _logger( Logger::instance() )
{
	this->_logger.info( "Save Manager Created" );
	this->findSaves();
}
SaveManager::~SaveManager()
{
	this->_logger.info( "Save Manager Deleted" );
}

bool SaveManager::save(){ return true; }
bool SaveManager::load(){ return true; }

void SaveManager::findSaves()
{
	this->_logger.info( "Generating Save List" );
	std::vector< SaveItem > saves;
	if( ! std::filesystem::is_directory( SAVE_LOCATION ) )
	{
		this->_logger.warning( "Unable to load save directory" );
		return;
	}

	// for( const auto& entry : std::filesystem::directory_iterator( SAVE_LOCATION ) )
	// {
	// 	saves.push_back( SaveItem(entry.path().filename() ) );
	// }
	this->saves = saves;
}

void SaveManager::list()
{
	int saves = 0;
	std::cout << "Save Files:\n";
	for( const SaveItem& save : this->saves )
	{
		std::cout <<"\t"<< ++saves << ": " << save << std::endl;
	}
}
