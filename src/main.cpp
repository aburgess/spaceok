#include "main.h"

#ifndef __TEST_RUNNER__

int main( int argc, char* argv[] ) {

	// Initialise
	Logger& logger = Logger::instance();

	// Seed the random number generator
	srand(static_cast<unsigned int>(time(nullptr)));


	GameEngine* game = new GameEngine();

	// Main Loop
	game->run();

	delete game;
	
	// Return 
	logger.info( "Process Ending" );
	return 0;
}


#else // __TEST_RUNNER__
//int /*WINAPI*/ WinMain(/*HINSTANCE hInstance, HINSTANCE, LPSTR, int*/) {
//	main( 0, nullptr );
//	return 0;
//}
#endif // __TEST_RUNNER__
