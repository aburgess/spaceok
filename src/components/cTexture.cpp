#include "components/cTexture.h"

cTexture::cTexture()
	: _texture( NULL ),
	_logger( Logger::instance() ),
	_width( 0 ), _height( 0 )
{}

cTexture::~cTexture(){ this->free(); }

bool cTexture::loadFromFile( std::string path )
{
	this->free();
	SDL_Texture* texture = NULL;
	SDL_Surface* surface = IMG_Load( path.c_str() );
	if( surface == NULL )
		{ printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() ); }
	else
	{
		SDL_SetColorKey( surface, SDL_TRUE, SDL_MapRGB( surface->format, 0xFF, 0x00, 0xFF ) );

		texture = SDL_CreateTextureFromSurface( Window::renderer, surface );
		if( texture == NULL )
			{ printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() ); }
		else
		{
			this->_width = surface->w;
			this->_height = surface->h;
		}

		SDL_FreeSurface( surface );
	}
	this->_texture = texture;
	this->setBlendMode( SDL_BLENDMODE_BLEND );
	return texture != NULL;
}

void cTexture::free()
{
	if( this->_texture != NULL )
	{
		_texture = NULL;
		_width = 0;
		_height = 0;
	}
}

void cTexture::setColour( Colour colour ) { this->setColour( colour.getRGBA().r, colour.getRGBA().g, colour.getRGBA().b ); }
void cTexture::setColour( uint8_t red, uint8_t green, uint8_t blue ) { SDL_SetTextureColorMod( this->_texture, red, green, blue ); }

void cTexture::setBlendMode( SDL_BlendMode blending ) { SDL_SetTextureBlendMode( this->_texture, blending ); }
void cTexture::setAlpha( uint8_t alpha ) { SDL_SetTextureAlphaMod( this->_texture, alpha ); }

void cTexture::render( SDL_Renderer *renderer, Vec2 pt,  SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip )
{
	if( this->_width && this->_height )
	{
		SDL_Rect quad = { (int)pt.x, (int)pt.y, this->_width, this->_height };
		if( clip != NULL )
		{
			quad.w = clip->w;
			quad.h = clip->h;
		}
		SDL_RenderCopyEx( renderer, this->_texture, clip, &quad, angle, center, flip );
	}
}