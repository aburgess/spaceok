#include "components/cFontfile.h"

bool cFontfile::ttf_isInit = false;

cFontfile::cFontfile()
	: cTexture()
{
	if( ! cFontfile::ttf_isInit )
	{
		if( TTF_Init() == -1 )
			{ printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() ); }
		else { cFontfile::ttf_isInit = true; }
	}
}


bool cFontfile::loadFromFile( std::string path )
{
	this->_logger.info( "Loading Font: "+path );
	this->free();
	SDL_Texture* texture = NULL;
	this->_font = TTF_OpenFont( path.c_str(), this->_fontsize );
	if( this->_font == NULL )
		{ printf( "Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError() ); }
	else
	{

	}
	return texture != NULL;
}

void cFontfile::renderText( std::string str )
{
	free();

	SDL_Surface* textSurface = TTF_RenderText_Solid( this->_font, str.c_str(), this->_col );

	if( textSurface == NULL )
		{ /* error */ return; }
	this->_texture = SDL_CreateTextureFromSurface( Window::renderer, textSurface );
	if( ! this->_texture )
		{ /* error */ return; }

	this->_width = textSurface->w;
	this->_height = textSurface->h;

	SDL_FreeSurface( textSurface );
}

void cFontfile::setColour( Colour colour ) { this->_colour = colour; }
void cFontfile::setColour( uint8_t red, uint8_t green, uint8_t blue ) {
	this->_colour = Colour( red, green, blue );
}