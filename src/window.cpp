#include "window.h"
#include "components/cFontfile.h"
#include "components/cTexture.h"

bool Window::sdlIsInit = false;
SDL_Renderer* Window::renderer = NULL;
RGBA Window::KeyColour = { 0xFF, 0x00, 0xFF, 0xFF };
RGBA Window::RenderColour = { 0xFF, 0xFF, 0xFF, 0xFF };

Window::Window()
	: _logger( Logger::instance() ), _hasInit( false )
{
	this->initialise();
	this->_window = nullptr;
	this->_screenSurface = nullptr;
	this->_renderSurface = nullptr;
	this->_background = new cTexture();
	this->_character = new cTexture();
	this->_arrow = new cTexture();
	this->_water = new cTexture();
	this->_fontFile = new cFontfile();
	this->_textureSprite = new cTexture();

	this->_screenModulation = Colour( 0x88, 0x99, 0xff, 0xFF );
}
Window::~Window()
{
}

void Window::initialise()
{
	if( !Window::sdlIsInit )
	{
		if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
		{
			// printf( "SDL could not initialize! SDL_Error: \n%s\n", SDL_GetError() );
			this->_logger.error( "Failed to initialise SDL" );
			return;
		}
		Window::sdlIsInit = true;
	}

	this->_toClose = false;
	this->_hasInit = true;
	this->_frame = 0;
}

SDL_Texture* Window::loadTexture( std::string path )
{
	SDL_Texture* texture = NULL;
	SDL_Surface* surface = this->loadSurface( path );

    if( surface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
		return nullptr;
	}
	texture = SDL_CreateTextureFromSurface( this->_renderer, surface );
	if( texture == NULL )
		{ printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() ); }

	SDL_FreeSurface( surface );
	return texture;	
}

SDL_Surface* Window::loadSurface( std::string path )
{
	//Load splash image
    SDL_Surface* optimizedSurface = NULL;
	SDL_Surface* surface = IMG_Load( path.c_str() );
	if( surface == NULL )
	{
		this->_logger.error( "Unable to load surface" );
		return nullptr;
	} else {
		std::cout << "optimising...";
		optimizedSurface = SDL_ConvertSurface(
			surface, this->_screenSurface->format, 0
		);
		std::cout << "finished" << std::endl;
		if( optimizedSurface == NULL )
			{ this->_logger.error( "Unable to optimise image" ); }
	}

	SDL_FreeSurface( surface );
	return optimizedSurface;
}

void Window::loadMedia()
{
	this->_background->loadFromFile( "assets/background.png" );
	this->_character->loadFromFile( "assets/person.png" );
	this->_arrow->loadFromFile( "assets/arrow.png" );
	this->_fontFile->loadFromFile( "assets/MonoRegular-jEmAR.ttf" );
	this->_fontFile->setColour( 255, 255, 0 );
	if( this->_water->loadFromFile( "assets/water_animated.png" ) )
	{
		this->_water->setAlpha( 164 );

		// Set Frame 1
		this->_waterTicks[ 0 ].x =   0;
		this->_waterTicks[ 0 ].y =   0;
		this->_waterTicks[ 0 ].w = 100;
		this->_waterTicks[ 0 ].h = 100;

		//Set top right sprite
		this->_waterTicks[ 1 ].x = 100;
		this->_waterTicks[ 1 ].y =   0;
		this->_waterTicks[ 1 ].w = 100;
		this->_waterTicks[ 1 ].h = 100;

		// Set Frame 3
		this->_waterTicks[ 2 ].x = 200;
		this->_waterTicks[ 2 ].y =   0;
		this->_waterTicks[ 2 ].w = 100;
		this->_waterTicks[ 2 ].h = 100;

		// Set Frame 4
		this->_waterTicks[ 3 ].x = 300;
		this->_waterTicks[ 3 ].y =   0;
		this->_waterTicks[ 3 ].w = 100;
		this->_waterTicks[ 3 ].h = 100;	
	}

	if( this->_textureSprite->loadFromFile( "assets/texture_test.png" ) )
	{
		this->_textureSprite->setAlpha( 128 );

		//Set top left sprite
		this->_sprites[ 0 ].x =   0;
		this->_sprites[ 0 ].y =   0;
		this->_sprites[ 0 ].w = 100;
		this->_sprites[ 0 ].h = 100;
		// this->_sprites[ 0 ].setAlpha( 64 );

		//Set top right sprite
		this->_sprites[ 1 ].x = 100;
		this->_sprites[ 1 ].y =   0;
		this->_sprites[ 1 ].w = 100;
		this->_sprites[ 1 ].h = 100;
		// this->_sprites[ 1 ].setAlpha( 128 );

		//Set bottom left sprite
		this->_sprites[ 2 ].x =   0;
		this->_sprites[ 2 ].y = 100;
		this->_sprites[ 2 ].w = 100;
		this->_sprites[ 2 ].h = 100;
		// this->_sprites[ 2 ].setAlpha( 192 );

		//Set bottom right sprite
		this->_sprites[ 3 ].x = 100;
		this->_sprites[ 3 ].y = 100;
		this->_sprites[ 3 ].w = 100;
		this->_sprites[ 3 ].h = 100;
		// this->_sprites[ 3 ].setAlpha( 255 );
	}
}
void Window::loadMedia( std::string path )
{
	//Load splash image
	this->_activeTexture = this->loadTexture( path.c_str() );
}

void Window::update() {
	SDL_SetRenderDrawColor(
		this->_renderer,
		Window::RenderColour.r,
		Window::RenderColour.g,
		Window::RenderColour.b,
		Window::RenderColour.a
	);
	SDL_RenderClear( this->_renderer );

	SDL_Rect bg = this->_createViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
	this->_background->setColour( this->_screenModulation );
	this->_background->render( this->_renderer, {0,0} );
	this->_character->render( this->_renderer, {SCREEN_HEIGHT/3,SCREEN_HEIGHT/3} );
	// this->_textureSprite->render( this->_renderer, {0,0} );

	//Render top left sprite
	this->_textureSprite->setAlpha( this->_alpha );
	this->_textureSprite->render( this->_renderer, {0, 0}, &this->_sprites[ 0 ] );
	//Render top right sprite
	this->_textureSprite->render( this->_renderer, {SCREEN_WIDTH - this->_sprites[ 1 ].w, 0}, &this->_sprites[ 1 ] );
	//Render bottom left sprite
	this->_textureSprite->render( this->_renderer, {0, SCREEN_HEIGHT - this->_sprites[ 2 ].h}, &this->_sprites[ 2 ] );
	//Render bottom right sprite
	this->_textureSprite->render( this->_renderer, {SCREEN_WIDTH - this->_sprites[ 3 ].w, SCREEN_HEIGHT - this->_sprites[ 3 ].h}, &this->_sprites[ 3 ] );

	
	SDL_Rect core = this->_createViewport( SCREEN_WIDTH / 4, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 );
	
	// Render Example Primatives
	this->_drawRect( SCREEN_WIDTH / 4, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 );
	this->_drawRect( 0, 0, 10, 10 );
	this->_drawLine( 0, SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT / 2 );
	for( uint16_t i = 0; i < SCREEN_HEIGHT; i += 10 )
	{
		this->_drawPoint( { SCREEN_WIDTH / 2, i } );
	}

	// Water
	this->_water->render( this->_renderer, { 0, 0 }, &this->_waterTicks[ this->_frame % this->_waterFrames ] );


	SDL_Rect overlay = this->_createViewport( 0, 0, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 );
	this->_arrow->render( this->_renderer, {100, 100}, NULL, this->_frame % 360 );

	const int hexRad = 10;
	const int hexSpacing = 1;
	const int drawHexRad = hexRad + hexSpacing;
	for( int i = 0; i < 20; i++ )
	{
		for( int j = 0; j < 20; j++ )
		{
			this->_drawHex( {
				i * ( drawHexRad * sqrt(3) ) + ( j % 2 == 0 ? 0 : ( drawHexRad * sqrt(3) ) / 2 ),
				j * ( drawHexRad * 3 / 2 )
			} , hexRad );

		}

	}

	// SDL_RenderCopy( this->_renderer, this->_activeTexture, NULL, NULL );

	overlay = this->_createViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
	this->_fontFile->renderText( std::to_string(this->fps) );
	this->_fontFile->render( this->_renderer, {50,50} );

	SDL_RenderPresent( this->_renderer );
}

SDL_Rect Window::_drawRect( int x, int y, uint16_t width, uint16_t height )
{
	SDL_Rect fillRect = { x, y, width, height };
	SDL_SetRenderDrawColor( this->_renderer, 0xFF, 0x00, 0x00, 0xFF );        
	SDL_RenderFillRect( this->_renderer, &fillRect );

	SDL_Rect outlineRect = { x, y, width, height };
	SDL_SetRenderDrawColor( this->_renderer, 0x00, 0xFF, 0x00, 0xFF );        
	SDL_RenderDrawRect( this->_renderer, &outlineRect );
	return fillRect;
}
void Window::_drawLine( int16_t x1, int16_t y1, int16_t x2, int16_t y2 )
{
	SDL_SetRenderDrawColor( this->_renderer, 0x00, 0x00, 0xFF, 0xFF );        
	SDL_RenderDrawLine( this->_renderer, x1, y1, x2, y2 );
}
void Window::_drawPoint( Vec2 point )
{
	SDL_SetRenderDrawColor( this->_renderer, 0xFF, 0xFF, 0x00, 0xFF );
	SDL_RenderDrawPoint( this->_renderer, point.x, point.y );
}
void Window::_drawHex( Vec2 point, uint16_t radius )
{
	const float halfWidth = (sqrt(3) * radius) / 2.0;
	const float quaterHeight = radius / 2.0;
	this->_drawLine( (int)point.x, (int)point.y + radius, (int)point.x + halfWidth, (int)point.y + quaterHeight );
	this->_drawLine( (int)point.x + halfWidth, (int)point.y + quaterHeight, (int)point.x + halfWidth, (int)point.y - quaterHeight );
	this->_drawLine( (int)point.x + halfWidth, (int)point.y - quaterHeight, (int)point.x, (int)point.y - radius );

	this->_drawLine( (int)point.x, (int)point.y + radius, (int)point.x - halfWidth, (int)point.y + quaterHeight );
	this->_drawLine( (int)point.x - halfWidth, (int)point.y + quaterHeight, (int)point.x - halfWidth, (int)point.y - quaterHeight );
	this->_drawLine( (int)point.x - halfWidth, (int)point.y - quaterHeight, (int)point.x, (int)point.y - radius );
}


void Window::spawn()
{
	this->_window = SDL_CreateWindow(
		"Space OK",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		SDL_WINDOW_SHOWN
	);
	if( this->_window == NULL )
	{
		printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
	} else {
		if( Window::renderer == NULL )
		{
			this->_renderer = SDL_CreateRenderer( this->_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC  );
			// this->_renderer = SDL_CreateRenderer( this->_window, -1, SDL_RENDERER_ACCELERATED   );
			if( this->_renderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
			} else {
				SDL_SetRenderDrawColor(
					this->_renderer,
					Window::RenderColour.r,
					Window::RenderColour.g,
					Window::RenderColour.b,
					Window::RenderColour.a
				);
			}
			Window::renderer = this->_renderer;
		} else {
			this->_renderer = Window::renderer;
		}


		int imgFlags = IMG_INIT_PNG;
		if( !( IMG_Init( imgFlags ) & imgFlags ) )
		{
			this->_logger.error( "SDL_image could not initialize! SDL_image Error" );
		}

		//Get window surface
		this->_screenSurface = SDL_GetWindowSurface( this->_window );


		//Fill the surface white
		SDL_FillRect( this->_screenSurface, NULL, SDL_MapRGB( this->_screenSurface->format, 0xFF, 0x00, 0xFF ) );

		//Update the surface
		SDL_UpdateWindowSurface( this->_window );
	}
}

void Window::processInput()
{
}

void Window::clean()
{
	// Clean Surfaces
	SDL_FreeSurface( this->_screenSurface );
	SDL_FreeSurface( this->_renderSurface );
	this->_screenSurface = nullptr;
	this->_renderSurface = nullptr;

	// Clean Texure
	SDL_DestroyTexture( this->_activeTexture );
	this->_activeTexture = nullptr;
	delete this->_background;
	delete this->_character;
	delete this->_textureSprite;

	//Destroy window
	SDL_DestroyRenderer( this->_renderer );
	SDL_DestroyWindow( this->_window );
	this->_window = nullptr;
	this->_renderer = nullptr;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();

	this->_logger.info( "Window Closed and clean" );
}

SDL_Rect Window::_createViewport( int16_t x, int16_t y, int16_t w, int16_t h )
{
	SDL_Rect viewport;
	viewport.x = x;
	viewport.y = y;
	viewport.w = w;
	viewport.h = h;
	SDL_RenderSetViewport( this->_renderer, &viewport );
	return viewport;
}