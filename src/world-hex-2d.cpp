#include "world-hex-2d.h"
WorldHex2D::WorldHex2D() : WorldHex2D( 5 ) {}
WorldHex2D::WorldHex2D( uint8_t radius )
	: WorldHex2D(
			radius % 2 == 0 ? radius + 1 : radius,
			radius % 2 == 0 ? radius + 1 : radius,
			radius % 2 == 0 ? radius + 1 : radius
) {}

WorldHex2D::WorldHex2D( uint8_t width, uint8_t height, uint8_t depth)
	: _logger( Logger::instance() ),
	_width( width ), _height( height ), _depth( depth )
{
	this->_logger.info( "Creating Hex Map" );
	this->generateWorld();
}

WorldHex2D::~WorldHex2D()
{
	this->_logger.info( "Cleaning world" ); 	

	// Clean Chunk Map
	for( const auto& [keyX, value1] : this->chunks_)
	{
		for( const auto& [keyY, value2] : value1)
		{
			for( const auto& [keyZ, chunk] : value2)
				{ delete chunk; }
		}
	}
}
void WorldHex2D::generateWorld()
{
	this->_logger.info( "Generating initial world" );
	
	this->_minS = this->_width / -2;	// s
	this->_minQ = this->_height / -2;	// q
	this->_minR = this->_depth / -2;	// r
	this->_maxS = ( this->_width / 2 ) + ( this->_width % 2 == 0 ? 0 : 1 );
	this->_maxQ = ( this->_height / 2 ) + ( this->_height % 2 == 0 ? 0 : 1 );
	this->_maxR = ( this->_depth / 2 ) + ( this->_depth % 2 == 0 ? 0 : 1 );

	for( int s = this->_minS; s < this->_maxS; s++ )
	{
		for( int q = this->_minQ; q < this->_maxQ; q++ )
		{
			for( int r = this->_minR; r < this->_maxR; r++ )
			{
				if( s + q + r == 0 )
					{ this->createChunkAtPoint( s, q, r ); }
			}
		}
	}
}

Chunk* WorldHex2D::createChunkAtPoint( int s, int q, int r )
{
	Chunk* chunk = new Chunk( s, q, r );
	this->chunks_[s][q][r] = chunk;

	if( s < this->_minS ) this->_minS = s;
	if( q < this->_minQ ) this->_minQ = q;
	if( r < this->_minR ) this->_minR = r;
	if( s > this->_maxS ) this->_maxS = s;
	if( q > this->_maxQ ) this->_maxQ = q;
	if( r > this->_maxR ) this->_maxR = r;

	return chunk;
}


Chunk* WorldHex2D::showChunk( int s, int q, int r )
{
	Chunk* chunk = this->getChunk( s, q, r );
	if( !chunk )
	{
		std::cout
			<< " Chunk Missing: [ "
			<< s << ", "
			<< q << ", "
			<< r << "] "
			<< std::endl;
		return nullptr;
	}
	
	// Get Neighbours
	Chunk * tr = this->getChunk( s+1, q, r-1 );
	Chunk * xr = this->getChunk( s, q+1, r-1 );
	Chunk * br = this->getChunk( s-1, q+1, r );
	Chunk * bl = this->getChunk( s-1, q, r+1 );
	Chunk * xl = this->getChunk( s, q-1, r+1 );
	Chunk * tl = this->getChunk( s+1, q-1, r );

	// Visualise
	std::cout << chunk->getString() << std::endl;

	std::cout << this->visualiseBlock( chunk, tl, tr, xr, br, bl, xl );

	return chunk;
}
std::string WorldHex2D::visualiseBlock(
	Chunk* core,
	Chunk* topLeft,
	Chunk* topRight,
	Chunk* right,
	Chunk* bottomRight,
	Chunk* bottomLeft,
	Chunk* left
) {
	int coreVal = core ? core->valVisual() : 0;
	int tlVal = topLeft ? topLeft->valVisual() : 0;
	int trVal = topRight ? topRight->valVisual() : 0;
	int xrVal = right ? right->valVisual() : 0;
	int brVal = bottomRight ? bottomRight->valVisual() : 0;
	int blVal = bottomLeft ? bottomLeft->valVisual() : 0;
	int xlVal = left ? left->valVisual() : 0;
	std::cout <<
		" " << this->colouriseValue(tlVal) << " " << this->colouriseValue(trVal) << std::endl
		<< this->colouriseValue(xlVal) << " " << this->colouriseValue(coreVal, 1) << " " << this->colouriseValue(xrVal) << std::endl
		<<" " << this->colouriseValue(blVal) << " " << this->colouriseValue(brVal) << std::endl;
	return "";
}

Chunk* WorldHex2D::getChunk( int chunkS, int chunkQ, int chunkR )
{
	auto chS = chunks_.find( chunkS );
	if( chS != chunks_.end() )
	{
		auto chQ = chS->second.find( chunkQ );
		if( chQ != chS->second.end() )
		{
			auto chR = chQ->second.find( chunkR );
			if( chR != chQ->second.end() )
				{ return chR->second; }
		}
	}

	return nullptr;
}

Chunk* WorldHex2D::moveInto( int s, int q, int r )
{
	Chunk * core = this->getChunk( s, q, r );
	Chunk * tr = this->getChunk( s+1, q, r-1 );
	Chunk * xr = this->getChunk( s, q+1, r-1 );
	Chunk * br = this->getChunk( s-1, q+1, r );
	Chunk * bl = this->getChunk( s-1, q, r+1 );
	Chunk * xl = this->getChunk( s, q-1, r+1 );
	Chunk * tl = this->getChunk( s+1, q-1, r );
	
	if( !core ) core = this->createChunkAtPoint( s, q, r );
	if( !tr  )tr = this->createChunkAtPoint( s+1, q, r-1 );
	if( !xr ) xr = this->createChunkAtPoint( s, q+1, r-1 );
	if( !br ) br = this->createChunkAtPoint( s-1, q+1, r );
	if( !bl ) bl = this->createChunkAtPoint( s-1, q, r+1 );
	if( !xl ) xl = this->createChunkAtPoint( s, q-1, r+1 );
	if( !tl ) tl = this->createChunkAtPoint( s+1, q-1, r );

	return core;
}

int WorldHex2D::getMidS(){ return this->_width / 2; }
int WorldHex2D::getMidQ(){ return this->_height / 2; }
int WorldHex2D::getMidR(){ return this->_depth / 2; }
Chunk* WorldHex2D::getMidChunk(){
	return this->getChunk(
		getMidS(),
		getMidQ(),
		getMidR()
	);
}

void WorldHex2D::visualise()
{
	for( int s = this->_maxS; s >= this->_minS; s-- )
	{
		for( int i = abs(s); i > 0; i-- )
			{ std::cout << "\u2006"; }
		// if( s % 2 != 0 ) { std::cout << "\u2006"; } 
		for( int q = this->_minQ; q < this->_maxQ; q++ )
		{
			for( int r = this->_minR; r < this->_maxR; r++ )
			{
				if( s + q + r == 0 )
				{
					Chunk * chunk = this->getChunk( s,q,r );
					if( chunk )
					{
						std::cout << " " << colouriseValue( chunk->valVisual() );
//						std::cout<<" \u2b21";
					} else {
						std::cout <<" ";
					}
					// std::cout<<BLK<<" \u2b21";
				}
			}
		}
		std::cout<<std::endl;
	}
	return;
}

std::string WorldHex2D::colouriseValue( uint8_t val, int isSolid )
{
	std::string valColour  = "";
	if( val == 0 ) valColour = BLK;
	else if( val < 2 ) valColour = BLU;
	else if( val < 4 ) valColour = CYN;
	else if( val < 6 ) valColour = YEL;
	else if( val < 8 ) valColour = RED;
	else valColour = MAG;
	valColour =  valColour
		+ (isSolid > 0 ? "\u2b22" : "\u2b21")
		+ CRESET;
	return valColour;
}

void WorldHex2D::visualiseBlocky()
{
	for( uint8_t s = 0; s < this->_width; s++ )
	{
		for( uint8_t q = 0; q < this->_height; q++ )
		{
			for( uint8_t r = 0; r < this->_depth; r++ )
			{
				Chunk* chunk = getChunk( s, q, r );
				if( chunk )
					{ std::cout << colouriseValue( chunk->valVisual(), 1 ); }
			}
			std::cout << std::endl;
		}
		std::cout << "#######" << std::endl;
	}
	std::cout << "TODO" << std::endl;
}
