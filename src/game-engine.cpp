#include "game-engine.h"


GameEngine::GameEngine()
	: _logger( Logger::instance() )
{
	this->_logger.info( "Creating GameEngine Object" );
}
GameEngine::~GameEngine()
{
	this->_logger.info( "Cleaning GameEngine Object" );
	this->clean();
}
void GameEngine::clean()
{
	delete this->_window;
}
void GameEngine::close(){ this->isRunning = false; }
void GameEngine::init()
{
	this->_window = new Window();
	this->_window->spawn();
	this->_window->loadMedia();
	this->_window->update();

	this->isRunning = false;
	this->_currScene = "";
	this->_state = Initialise;
	// this->_saveManager.list();
}
void GameEngine::changeScene( std::string label, Scene* scene )
{
	this->_scenes[ label ] = scene;
	this->_currScene = label;
}
void GameEngine::changeScene( std::string label )
	{ this->_currScene = label; }

Scene* GameEngine::currentScene()
	{ return this->_scenes[this->_currScene]; }

void GameEngine::run()
{
	this->init();
	this->isRunning = true;
	this->_frame = 0;
	// std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	// std::time_t _frametime = std::chrono::system_clock::to_time_t(now);
	while( this->isRunning ) { this->update(); }
}

void GameEngine::update()
{
	// this->_window->processInput();
	this->handleInput();

	this->_entityManager.update();
	const std::chrono::time_point<std::chrono::high_resolution_clock> now
		 = std::chrono::high_resolution_clock::now();
	const auto cTime = now.time_since_epoch().count() / 1000000;
	const auto fTime = this->_frametime;


	++this->_frame;
	// std::cout << now << "|"<< this->_frametime << std::endl;
	if ( cTime - fTime > 25 && this->_frame > 10 )
	{
		double fps = (double) this->_frame / (cTime - fTime) * 1000;
		// printf( "Frame: %i at %.2f fps\n", this->_frame, fps );
		this->_frametime = cTime;
		this->_frame = 0;
		this->_window->fps = fps;
	}
	this->_window->update();
}

void GameEngine::handleInput()
{
	SDL_Event e;
	while( SDL_PollEvent( &e ) )
	{
		// std::cout << "event" << std::endl;
		if( e.type == SDL_QUIT ) this->close();
		else if( e.type == SDL_KEYDOWN )
		{
			switch( e.key.keysym.sym )
			{
				case SDL_QUIT: this->close(); break;
				case SDLK_ESCAPE: this->close(); break;
			}
		} else if(
			e.type == SDL_MOUSEMOTION ||
			e.type == SDL_MOUSEBUTTONDOWN ||
			e.type == SDL_MOUSEBUTTONUP
		) { SDL_GetMouseState( &(this->m_mX), &(this->m_mY) ); }
	}

}
