#include "maths/vec2.h"

Vec2::Vec2()
	: Vec2( 0.0, 0.0 ) {}
Vec2::Vec2( float xIn, float yIn )
	: x( xIn ), y( yIn )
{};
		
bool Vec2::operator != ( const Vec2 rhs ) const {
	return !(
		abs( this->x - rhs.x ) < 0.01
	) && (
		abs( this->y - rhs.y ) < 0.01
	);
}
bool Vec2::operator == ( const Vec2 rhs ) const {
	return (
		abs( this->x - rhs.x ) < 0.01
	) && (
		abs( this->y - rhs.y ) < 0.01
	);
}
Vec2 Vec2::operator + ( const Vec2 rhs ) const { return Vec2( this->x + rhs.x, this->y + rhs.y ); }
Vec2 Vec2::operator - ( const Vec2 rhs ) const { return Vec2( this->x - rhs.x, this->y - rhs.y ); }
Vec2 Vec2::operator * ( const Vec2 rhs ) const { return Vec2( this->x * rhs.x, this->y * rhs.y ); }
Vec2 Vec2::operator / ( const Vec2 rhs ) const { return Vec2( this->x / rhs.x, this->y / rhs.y ); }
Vec2 Vec2::operator * ( const float scale ) const { return Vec2( this->x * scale, this->y * scale ); }
Vec2 Vec2::operator / ( const float scale ) const { return Vec2( this->x / scale, this->y / scale ); }
void Vec2::operator += ( const Vec2 & rhs ) { this->x += rhs.x; this->y += rhs.y; }
void Vec2::operator -= ( const Vec2 & rhs ) { this->x -= rhs.x; this->y -= rhs.y; }
void Vec2::operator *= ( const Vec2 & rhs ) { this->x *= rhs.x; this->y *= rhs.y; }
void Vec2::operator *= ( const float scale ) { this->x *= scale; this->y *= scale; }
void Vec2::operator /= ( const Vec2 & rhs ) { this->x /= rhs.x; this->y /= rhs.y; }
void Vec2::operator /= ( const float scale ) { this->x /= scale; this->y /= scale; }

float Vec2::dist( const Vec2 &rhs ) const
{
	float x = this->x - rhs.x;
	float y = this->y - rhs.y;
	return sqrtf( x*x + y*y );
}
float Vec2::length()
	{ return sqrtf( this->x*this->x + this->y*this->y ); }
void Vec2::normalise()
{
	float mag = this->length();
	this->x /= mag;
	this->y /= mag;
}
