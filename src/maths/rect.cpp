#include "maths/rect.h"

Rect::Rect( float x, float y, float w, float h )
	: Rect( Vec2(x, y), w, h )
{}
Rect::Rect( Vec2 pt, float w, float h )
	: _pt( pt ),
	_halfWidth( w / 2 ),
	_halfHeight( h / 2 )
{}

bool Rect::isOverlap( Vec2 pt )
{
	return pt.x > ( pt.x - _halfWidth )
		&& pt.x < ( pt.x + _halfWidth )
		&& pt.y > ( pt.y - _halfHeight )
		&& pt.y < ( pt.y + _halfHeight );
}
bool Rect::isOverlap( Rect rect )
	{ return this->overlap_horizontal( rect ) && this->overlap_vertical( rect ); }


bool Rect::overlap_horizontal( Rect rect )
{
	return abs( this->_pt.x - rect._pt.x )
		< this->_halfWidth + rect._halfWidth;
}
bool Rect::overlap_vertical( Rect rect )
{
	return abs( this->_pt.y - rect._pt.y )
		< this->_halfHeight + rect._halfHeight;
}

