#include "entity-manager.h"

EntityManager::EntityManager()
	: _logger( Logger::instance() )
{ this->_logger.info( "Creating entity manager" ); }
EntityManager::~EntityManager()
{
	while( this->_entities.size() )
	{
		auto entity = this->_entities.back();
		this->_entities.pop_back();
		delete &entity;
	}

	this->_logger.info( "Deleting entity manager" );
}
std::shared_ptr<Entity> EntityManager::addEntity( const std::string& tag )
{
	this->_logger.info( "Adding entity with tag "+tag ); 
	auto entity = std::shared_ptr< Entity >( new Entity( tag, this->nextId() ) );
	this->_entitiesToAdd.push_back( entity );
	return entity;
}

EntityVector& EntityManager::getEntities(){ return this->_entities; }
EntityVector& EntityManager::getEntities( const std::string& tag ){
	return this->_entities;
}

void EntityManager::update()
{
	for( auto entity : this->_entities )
	{
		if( entity->isDestroyed() )
		{
			/** @todo Implement this with a clean deletion taking into account iterator invalidation
			 */
			// Remove from this->entities;
			// Remove from this->entitiesMap;
		}
	}
	for( auto entity : this->_entitiesToAdd )
	{
		this->_entities.push_back( entity );
		this->_entityMap[entity->tag()].push_back( entity );
	}
	this->_entitiesToAdd.clear();
	return;
}

void EntityManager::removeEntity(){}
