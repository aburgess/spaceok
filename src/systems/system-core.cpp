#include "systems/system-core.h"


SystemsGeneric::SystemsGeneric()
	: _logger( Logger::instance() )
{ this->_logger.info( "Generic System Created" ); }
SystemsGeneric::~SystemsGeneric()
{ this->_logger.info( "Generic System Deleted" ); }
