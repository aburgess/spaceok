#include "chunk.h"


Chunk::Chunk( int x, int y, int z )
	: _logger( Logger::instance() ),
		_x(x), _y(y), _z(z)
{ this->generate(); }
Chunk::~Chunk()
{
	// this->_logger.info( "Deleting Chunk" );
}
void Chunk::generate(){ this->generate_random(); }
void Chunk::generate_random()
{
	// this->_logger.info( "Generating Chunk at "+this->getLocationString()+" - Random" );

	this->wealth = rand();
	this->weirdness = rand();
	this->density = rand();
	this->background_radiation = rand();
	this->background_tempreture = rand() % 512;
	this->radiation = rand();
	this->age = rand();
	this->goodness = rand();
	this->chaos = rand();
	this->magic = rand();
}
std::string Chunk::getLocationString() const
{
	return std::string("[")
			+std::to_string(this->_x)
			+std::string(",")
			+std::to_string(this->_y)
			+std::string(",")
			+std::to_string(this->_z)
			+std::string("]");
}
std::string Chunk::getWealthString() const { return std::to_string(this->wealth); }
std::string Chunk::getWeirdnessString() const { return std::to_string(this->weirdness); }
std::string Chunk::getDensityString() const { return std::to_string(this->density); }
std::string Chunk::getBackground_radiationString() const { return std::to_string(this->background_radiation); }
std::string Chunk::getBackground_tempretureString() const { return std::to_string(this->background_tempreture); }
std::string Chunk::getRadiationString() const { return std::to_string(this->radiation); }
std::string Chunk::getAgeString() const { return std::to_string(this->age); }

std::string Chunk::getString( ChunkQuery format ) const
	{ return "rest"; }

void Chunk::visualise() const { std::cout << this->getString() << std::endl; }
std::string Chunk::getString() const
{
	std::string str = "Chunk Details:"
		+std::string("\n\tLocation:\t")+this->getLocationString()
		+std::string("\n\tWealth:\t")+this->getWealthString()
		+std::string("\n\tWeirdness:\t")+this->getWeirdnessString()
		+std::string("\n\tDensity:\t")+this->getDensityString()
		+std::string("\n\tBackground Radiation:\t")+this->getBackground_radiationString()
		+std::string("\n\tBackground Tempreture:\t")+this->getBackground_tempretureString()
		+std::string("\n\tRadiation:\t")+this->getRadiationString()
		+std::string("\n\tAge:\t")+this->getAgeString();

	return str;
}

int Chunk::valVisual()
{
	int val = this->density * 10 / ( 0xFFFF );
	return val == 0 ? 1 : val;
}

std::ostream& operator<<( std::ostream& os, const Chunk& obj ) { return os << obj.getString(); }
