#ifndef __COMPONENT__SHAPE_H__
#define __COMPONENT__SHAPE_H__

#include "components/components-generic.h"
#include "maths/maths.h"

class cShape : public CComponentGeneric
{
	public:
		cShape( uint8_t verticies )
			: _verticies( verticies ) {}
	private:
		uint8_t _verticies;
};


#endif // __COMPONENT__SHAPE_H__
