#ifndef __COMPONENT__TRANSFORM_H__
#define __COMPONENT__TRANSFORM_H__

#include "components/components-generic.h"
#include "maths/maths.h"

class cTransform : public CComponentGeneric
{
	public:
		Vec2 position = { 0.0, 0.0 };
		Vec2 velocity = { 0.0, 0.0 };
		Vec2 scale = { 0.0, 0.0 };
		double angle = 0.0;

		cTransform(){}
		cTransform( const Vec2 &pos, const Vec2 &vel, float ang )
			: position(pos), velocity(vel), angle(ang) {}
};


#endif // __COMPONENT__TRANSFORM_H__
