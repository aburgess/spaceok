#ifndef __COMPONENTS__FONTFILE_H__
#define __COMPONENTS__FONTFILE_H__

#include <SDL_image.h>
#include <SDL_ttf.h>

#include "components/cTexture.h"
#include "colour.h"

class cFontfile : public cTexture
{
	static bool ttf_isInit;
	public:
		cFontfile();
		void renderText( std::string str );
		bool loadFromFile( std::string path );

		void setColour( Colour colour );
		void setColour( uint8_t red, uint8_t green, uint8_t blue );
	private:
		TTF_Font* _font = NULL;
		Colour _colour = Colour(0, 0, 0);
		SDL_Color _col = { 0xff, 0xff, 0x00 };
		uint8_t _fontsize = 28;
};


#endif // __COMPONENTS__FONTFILE_H__