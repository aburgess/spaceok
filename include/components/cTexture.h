#ifndef __COMPONENTS__TEXTURE_H__
#define __COMPONENTS__TEXTURE_H__

#ifndef console
#include <SDL.h>
#endif
#include <cstdio>
#include <string>
#include "components/components-generic.h"
#include "window.h"
#include "logger.h"
#include "colour.h"
#include "maths/vec2.h"

class cTexture : public CComponentGeneric
{
	public:
		cTexture();
		~cTexture();
		bool loadFromFile( std::string path );
		void free();
		void render(
			SDL_Renderer *renderer,
			Vec2 pt,
			SDL_Rect *clip = NULL,
			double angle = 0,
			SDL_Point* center = NULL,
			SDL_RendererFlip flip = SDL_FLIP_NONE
		);

		int getWidth(){ return this->_width; }
		int getHeight(){ return this->_height; }

		void setColour( Colour colour );
		void setColour( uint8_t red, uint8_t green, uint8_t blue );
		
		void setBlendMode( SDL_BlendMode blending );
		void setAlpha( uint8_t alpha );
	protected:
		Logger &_logger;
		SDL_Texture* _texture;
		uint16_t _width;
		uint16_t _height;
};

class cFontfile;

#endif // __COMPONENTS__TEXTURE_H__
