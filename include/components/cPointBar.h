#ifndef __COMPONENT__POINT_BAR_H__
#define __COMPONENT__POINT_BAR_H__

#include "components/components-generic.h"
#include "maths/maths.h"

class cPointBar : public CComponentGeneric
{
	public:
		cPointBar( uint16_t value )
			: _total(value), _current(value) {}
	private:
		uint16_t _total;
		uint16_t _current;
};


#endif // __COMPONENT__POINT_BAR_H__
