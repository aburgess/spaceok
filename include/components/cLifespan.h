#ifndef __COMPONENT__LIFESPAN_H__
#define __COMPONENT__LIFESPAN_H__

#include "components/components-generic.h"

class cLifespan : public CComponentGeneric
{
	public:
		uint16_t total;
		uint16_t remaining;
};


#endif // __COMPONENT__LIFESPAN_H__
