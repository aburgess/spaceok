#ifndef __COMPONENT__SCORE_H__
#define __COMPONENT__SCORE_H__

#include "components/components-generic.h"

class cScore : public CComponentGeneric
{
	public:
		uint16_t score;
};


#endif // __COMPONENT__SCORE_H__
