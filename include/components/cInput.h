#ifndef __COMPONENT__INPUT_H__
#define __COMPONENT__INPUT_H__

#include "components/components-generic.h"
#include "maths/maths.h"

class cInput : public CComponentGeneric
{
	public:
		bool up = false;
		bool down = false;
		bool left = false;
		bool right = false;
		bool shoot = false;
};


#endif // __COMPONENT__INPUT_H__
