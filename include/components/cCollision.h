#ifndef __COMPONENT__COLLISION_H__
#define __COMPONENT__COLLISION_H__

#include "components/components-generic.h"
#include "maths/maths.h"

class cCollision : public CComponentGeneric
{
	public:
		double radius = 0.0;

		cCollision(){}
		cCollision( float rad )
			: radius( rad ) {}
};


#endif // __COMPONENT__COLLISION_H__