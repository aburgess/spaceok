#ifndef __GAME_STATE__H__
#define __GAME_STATE__H__
enum GameState
{
	Uninitialised,
	Initialise,
	Menu,
	Active,
	Exit
};

#endif // __GAME_STATE__H__