#ifndef __SCENE_H__ 
#define __SCENE_H__ 

#include <map>
#include <string>
#include <vector>

#include "logger.h"
#include "asset.h"
#include "entities/entity.h"
#include "entity-manager.h"

class Scene{
	public:
		Scene();// : _logger( Logger::instance() ) {}
		~Scene();

		virtual void update() = 0;
		virtual void simulate() = 0;
		virtual void doAction() = 0;
		virtual void render() = 0;
		virtual void registerAction() = 0;
	protected:
		std::vector<Asset> _assets;
		Logger& _logger;

		EntityManager _entities;
		size_t _frame;

		bool _hasEnded = false;
		std::map< size_t, std::string> _actionMap;
};

#endif // __SCENE_H__ 
