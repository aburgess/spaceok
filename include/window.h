#ifndef __WINDOW_H__
#define __WINDOW_H__

#ifndef console
#include <SDL.h>
#include <SDL_image.h>
#endif

#include <cstdio>
#include <cstring>
#include <string>
#include "logger.h"
#include "colour.h"
#include "maths/vec2.h"
#include "game-engine.h"



// #include "components/cFontfile.h"
// #include "components/cTexture.h"

class cFontfile;
class cTexture;

const uint16_t SCREEN_HEIGHT = 600;
const uint16_t SCREEN_WIDTH = 800;

//Key press surfaces constants
enum KeyPressSurfaces
{
    KEY_PRESS_SURFACE_DEFAULT,
    KEY_PRESS_SURFACE_UP,
    KEY_PRESS_SURFACE_DOWN,
    KEY_PRESS_SURFACE_LEFT,
    KEY_PRESS_SURFACE_RIGHT,
    KEY_PRESS_SURFACE_TOTAL
};

class Window
{
	friend class GameEngine;

	public:
		static SDL_Renderer* renderer;
		static bool sdlIsInit;
		static RGBA KeyColour;
		static RGBA RenderColour;
		Window();
		~Window();

		void loadMedia( std::string path );
		void loadMedia();
		void update();
		void processInput();

		SDL_Texture* loadTexture( std::string path );

		bool isClosed() { return this->_toClose; }

		void spawn();
	protected:
		double fps = 0;
	private:
		bool _toClose;
		bool _hasInit;
		SDL_Surface* loadSurface( std::string path );
		Logger& _logger;
		SDL_Window* _window;
		size_t _frame = 0;
		SDL_Texture* _activeTexture;

		Colour _screenModulation;
		cTexture* _background;
		cTexture* _character;
		cTexture* _arrow;
		cTexture* _textureSprite;
		cFontfile* _fontFile;
		SDL_Rect _sprites[4];
		uint8_t _alpha = 0xff;

		cTexture* _water;
		const uint8_t _waterFrames = 4;
		SDL_Rect _waterTicks[4];

		SDL_Renderer* _renderer;
		SDL_Surface* _screenSurface;
		SDL_Surface* _renderSurface;

		SDL_Rect _drawRect( int x, int y, uint16_t width, uint16_t height );
		void _drawLine( int16_t x1, int16_t y1, int16_t x2, int16_t y2 );
		// void _drawLine( Vec2 from, Vec2 to );	
		void _drawPoint( Vec2 point );
		// void _drawPoint( int16_t x, int16_t y );
		void _drawHex( Vec2 point, uint16_t radius );

		SDL_Rect _createViewport( int16_t x, int16_t y, int16_t w, int16_t h );

		void initialise();
		void clean();
};


#endif // __WINDOW_H__
