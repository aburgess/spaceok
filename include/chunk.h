#ifndef CHUNK_H
#define CHUNK_H

#include <cstdint>
#include <cstdlib>
#include <string>
#include <cstdlib>
#include <iostream>

#include "logger.h"

enum ChunkQuery
{
	age, weirdness
};

class Chunk
{
	public:
		Chunk( int x, int y, int z );
		~Chunk();
		void generate();
		void visualise() const;
		std::string getString() const;
		std::string getString( ChunkQuery format ) const;
		std::string getLocationString() const;
		std::string getWealthString() const;
		std::string getWeirdnessString() const;
		std::string getDensityString() const;
		std::string getBackground_radiationString() const;
		std::string getBackground_tempretureString() const;
		std::string getRadiationString() const;
		std::string getAgeString() const;

		int valVisual();

	private:
		Logger& _logger;

		const int _x;
		const int _y;
		const int _z;

		uint16_t wealth;
		uint16_t weirdness;
		uint16_t density;
		uint16_t background_radiation;
		uint16_t background_tempreture;
		uint16_t radiation;
		uint16_t age;

		int8_t goodness;
		int8_t chaos;
		int8_t magic;

		void generate_random();

};

#endif // CHUNK_H
