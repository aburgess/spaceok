  #ifndef UTILS_LOGGER_H
#define UTILS_LOGGER_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <ctime>
#include <iomanip>

#include "ANSI-colour-codes.h"

enum LogLevel
{
	DEBUG, INFO, WARNING, ERR, FATAL
};

class Logger
{
	public:
		static Logger& instance();

		void setLogLevel( LogLevel level );
		
		void debug( const std::string& message );
		void info( const std::string& message );
		void warning( const std::string& message );
		void error( const std::string& message );
		void fatal( const std::string& message );


	private:
		Logger();
		Logger( const Logger& );// = delete;
		Logger& operator=(const Logger&) = delete;
		~Logger();

		LogLevel currentLogLevel;
		std::ofstream logFile;

		void log(LogLevel level, const std::string& message);
		std::string logLevelToString( LogLevel level );
		std::string getCurrentTimestamp();
		std::string getLevelColour( LogLevel level );
};

#endif // UTILS_LOGGER_H
