#ifndef __GAME_H__
#define __GAME_H__

#include <memory>
#include <map>
#include <string>
#include <vector>
#include <chrono>
#include <ctime>

#ifndef console
#include <SDL.h>
#endif
	
	// Return 

#include "logger.h"
#include "entity-manager.h"
#include "game-state.h"
#include "scene.h"
#include "window.h"
#include "asset.h"
#include "save-manager.h"
// #include "window.h"


class Window;

class GameEngine
{
	public:
		GameEngine();
		~GameEngine();
		void init();
		void run();
		void update();
		void close();
		void clean();
		Scene* currentScene();
		void changeScene( std::string label, Scene *scene );
		void changeScene( std::string label );
		std::vector< Asset& > getAssets();
		Window& getWindow();

		void sUserInput();

		bool isRunning;
		void handleInput();
	private:
		SaveManager _saveManager;
		GameState _state = Uninitialised;
		size_t _frame = 0;
		size_t _frametime = 0;
		EntityManager _entityManager;
		Logger& _logger;
		int m_mX = 0;
		int m_mY = 0;

		bool _paused = false;

		Window* _window;
		// std::vector<Asset> _assets;
		// std::map< std::string, Scene > _scenes;
		std::vector<Asset> _assets;	/** @todo : move this to scene */

		std::string _currScene;
		std::map< std::string, Scene* > _scenes;

		/* Systems */
		// void sMovement();
		// void sUserInput();
		// void sRender();
		// void sCollision();
};

#endif // __GAME_H__
