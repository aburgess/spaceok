#ifndef ELEMENT_H
#define ELEMENT_H

#include <cstdint>

class Element
{
	public:
		Element( uint8_t atomicNumber );
		~Element();

	private:
		uint8_t _protons;
};


#endif // ELEMENT_H
