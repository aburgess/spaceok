#ifndef __ENTITY_MANAGER_H__
#define __ENTITY_MANAGER_H__

#include <string>
#include <vector>
#include <memory>
#include <map>

#include "logger.h"
#include "entities/entity.h"

typedef std::vector<std::shared_ptr<Entity>> EntityVector;
typedef std::map< std::string, EntityVector > EntityMap;

class EntityManager
{
	public:
		EntityManager();
		~EntityManager();
		std::shared_ptr<Entity> addEntity( const std::string& tag );
		void removeEntity();
		void update();

		EntityVector& getEntities();
		EntityVector& getEntities( const std::string& tag );
	private:
		Logger& _logger;
		EntityVector _entities;
		EntityVector _entitiesToAdd;
		EntityMap _entityMap;
		size_t _totalEntities = 0;
		
		size_t nextId() { return this->_totalEntities++; }
};

#endif // __ENTITY_MANAGER_H__
