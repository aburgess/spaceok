#ifndef __SCENES__PLAY_H__
#define __SCENES__PLAY_H__

#include "scene.h"
#include "entities/entity.h"

class ScenePlay : public Scene
{
	public:
		void update();
		void simulate();
		void doAction();
		void render();
		void registerAction();
	
	private:
		std::string _levelPath;
		Entity* _player;

		// Systems
		// sAnimation();
};

#endif // __SCENES__PLAY_H__
