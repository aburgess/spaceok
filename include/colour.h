#ifndef __COLOUR_H__
#define __COLOUR_H__

#include <cstdint>

struct RGBA{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};

class Colour
{
	public:
		Colour();
		Colour( uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255 );
		~Colour();

		void brighten( uint8_t delta );
		void darken( uint8_t delta );

		RGBA getRGBA();
	private:
		RGBA _col;
	
};

#endif //__COLOUR_H__