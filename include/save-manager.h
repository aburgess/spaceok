#ifndef SAVEMANAGER_H
#define SAVEMANAGER_H

#include <string>
#include <iostream>
#include <filesystem>
#include <vector>

#include "logger.h"

class SaveItem
{
	public:
		SaveItem( std::string );
		~SaveItem();
		std::string getString() const;

	private:
		std::string _name;
		std::string _path;

};

class SaveManager
{
	public:
		SaveManager();
		~SaveManager();

		bool save();
		bool load();
		void list();

	private:
		Logger& _logger;
 		const std::string SAVE_LOCATION = "saves";
		void findSaves();

		std::vector< SaveItem > saves;
};


#endif // SAVEMANAGER_H
