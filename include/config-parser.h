#ifndef __CONFIG_PARSER_H__
#define __CONFIG_PARSER_H__

#include <string>
#include <iostream>
#include <filesystem>
#include <map>

#include "logger.h"

typedef std::pair<std::string,std::string> keyPair;
typedef std::map<std::string,std::string> ConfigMap;

class ConfigParser
{
	friend std::ostream& operator<<( std::ostream& os, const ConfigParser& obj );

	public:
		static ConfigParser UserSettings();

		~ConfigParser();
	
		ConfigMap userSettings(){ return this->readFile( "settings.cfg" ); }

		void set( std::string key, std::string value ); 
		std::string get( std::string key ); 

		std::string toString() const;

	private:
		std::string _path;
		ConfigParser( std::string path );
 		const std::string CONFIG_DIRECTORY = "config";
 		const char DELIMINATOR = ':';
 		const char COMMENT_CHAR = '#';
		Logger& _logger;

		ConfigMap _config;

		ConfigMap readFile( std::string path );
		// void writeConfigFile( std::map settings );
		
		std::string getKey( std::string line );
		std::string getValue( std::string line );
};

#endif //  __CONFIG_PARSER_H__

