#ifndef __ASSET_H__
#define __ASSET_H__

#include <string>

#include "logger.h"

class Asset
{
	public:
		Asset();
		~Asset();

		void loadMedia( std::string path );
	private:
		Logger& _logger;
};

#endif // __ASSET_H__
