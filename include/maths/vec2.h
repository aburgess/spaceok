#ifndef __MATHS__VEC2_H__
#define __MATHS__VEC2_H__

#include <cmath>


class Vec2
{
	public:
		float x = 0, y = 0;
		Vec2();
		Vec2( float xIn, float yIn );
		
		bool operator == ( const Vec2 rhs ) const;
		bool operator != ( const Vec2 rhs ) const;

		Vec2 operator + ( const Vec2 rhs ) const;
		Vec2 operator - ( const Vec2 rhs ) const;
		Vec2 operator * ( const Vec2 rhs ) const;
		Vec2 operator * ( const float scale ) const;
		Vec2 operator / ( const Vec2 rhs ) const;
		Vec2 operator / ( const float scale ) const;

		void operator += ( const Vec2 & rhs );
		void operator -= ( const Vec2 & rhs );
		void operator *= ( const Vec2 & rhs );
		void operator *= ( const float rhs );
		void operator /= ( const Vec2 & rhs );
		void operator /= ( const float rhs );

		float dist( const Vec2 &rhs ) const;
		float length();
		void normalise();
};

#endif // __MATHS__VEC2_H__
