#ifndef __MATHS_RECT_H__
#define __MATHS_RECT_H__

#include "maths/vec2.h"

class Rect
{
	public:
		Rect( float x, float y, float w, float h );
		Rect( Vec2 pt, float w, float h );
		~Rect(){}

		bool isOverlap( Vec2 pt );
		bool isOverlap( Rect rect );
		
		bool overlap_horizontal( Rect rect );
		bool overlap_vertical( Rect rect );

	private:
		Vec2 _pt;
		float _halfWidth;
		float _halfHeight;
};


#endif // __MATHS_RECT_H__
