#ifndef __ENTITIES__BUTTON
#define __ENTITIES__BUTTON

#include <SDL.h>

#include "entities/entity.h"
#include "maths/maths.h"
#include "components/cTexture.h"

class eButton : public Entity
{
	public:
		void setPosition( Vec2 pos );
		void handleEvent( SDL_Event* evt );
		void render();
	private:
		Vec2 _pos;
		cTexture _sprite;
};

#endif // __ENTITIES__BUTTON
