
#ifndef __ENTITIES__ENTITY_GENERIC_H
#define __ENTITIES__ENTITY_GENERIC_H

// #include "entity-manager.h"
#include "logger.h"
#include "components/cTransform.h"
#include "components/cScore.h"
#include "components/cShape.h"
#include "components/cCollision.h"

class Entity
{
	friend class EntityManager;
	public:
		~Entity();

		bool isActive() { return this->_active; } 
		bool isDestroyed() { return ! this->_active; } 
		void destroy() { this->_active = false; } 
		const size_t id() const { return this->_id; }

		// Component Variables	
		const std::string& tag() { return this->_tag; } 

	private:
		Entity( const std::string& tag, size_t id );	/** @todo make this private */
		Logger& _logger;
		cScore _score;
		cTransform _transform;
		cShape _shape = {0};
		cCollision _collision;

		bool _active = true;
		const size_t _id = 0;
		const std::string _tag = "Default";

};

#endif // __ENTITIES__ENTITY_GENERIC_H
