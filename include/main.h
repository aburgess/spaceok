#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <cstdint>

#define SDL_MAIN_HANDLED

#ifdef __TEST_RUNNER__
#ifndef DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#endif // DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "testlist.hpp"
#endif // __TEST_RUNNER__

enum GenerationMethod
{
	Random,
	Cave
};

#include "maths/maths.h"
#include "save-manager.h"
#include "game-state.h"
#include "world-hex-2d.h"
#include "chunk.h"
#include "config-parser.h"
#include "game-engine.h"
#include "logger.h"
#include "entities/entity.h"
#include "maths/maths.h"
#include "entity-manager.h"
#include "components/cTexture.h"
#include "window.h"


#endif // MAIN_H
