#ifndef WORLD_HEX_2D
#define WORLD_HEX_2D

#include <unordered_map>

#include "chunk.h"
#include "logger.h"
#include "ANSI-colour-codes.h"

#include <string>
#include <iostream>


class WorldHex2D
{
	public:
		WorldHex2D();
		WorldHex2D( uint8_t radius );
		WorldHex2D( uint8_t width, uint8_t height, uint8_t depth);
		~WorldHex2D();
		void generateWorld();

		Chunk* createChunkAtPoint( int s, int q, int r );
		Chunk* getChunk( int chunkS, int chunkQ, int chunkR );
		Chunk* moveInto( int chunkS, int chunkQ, int chunkR );

		Chunk* getMidChunk();
		int getMidS();
		int getMidQ();
		int getMidR();

		void visualise();
		void visualiseBlocky();
		Chunk* showChunk( int s, int q, int r );
	private:
		Logger& _logger;
		std::unordered_map<int, std::unordered_map<int, std::unordered_map<int, Chunk*>>> chunks_;
		std::string colouriseValue( uint8_t val, int isSolid = 0 );

		uint8_t _width;	// s
		uint8_t _height;	// q
		uint8_t _depth;	// r
		int _minS;	// s
		int _minQ;	// q
		int _minR;	// r
		int _maxS;	// s
		int _maxQ;	// q
		int _maxR;	// r
		
		std::string visualiseBlock(
			Chunk* core,
			Chunk* topLeft,
			Chunk* topRight,
			Chunk* right,
			Chunk* bottomRight,
			Chunk* bottomLeft,
			Chunk* left
		);
};

#endif // WORLD_HEX_2D
