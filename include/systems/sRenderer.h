#ifndef __SYSTEMS__RENDERER_H_
#define __SYSTEMS__RENDERER_H_

#include "systems/system-core.h"
#include "logger.h"

class sRenderer : public SystemsGeneric
{

	public:
		sRenderer();
		~sRenderer();
	private:
		Logger& _logger;
};

#endif // __SYSTEMS__RENDERER_H_
