#ifndef __SYSTEMS__SYSTEM_CORE_H_
#define __SYSTEMS__SYSTEM_CORE_H_

#include "logger.h"

class SystemsGeneric
{

	public:
		SystemsGeneric();
		~SystemsGeneric();
	private:
		Logger& _logger;
};

#endif // __SYSTEMS__SYSTEM_CORE_H_
