#include <iostream>
#include "doctest.h"


#include "maths/vec2.h"

TEST_CASE( "Testing Operator Comparisons" ) {
	float epsilon = 1e-10;

	Vec2 a( 1, 5 );
	Vec2 b( 2, 4 );
	float scale_a = 100;
	float scale_b = 0.5;
	float scale_c = 0;

	SUBCASE( "Addition" )
	{
		Vec2 c = a + b;
		CHECK( c.x == 3 );
		CHECK( c.y == 9 );
	}
	SUBCASE( "Multiplication" )
	{
		Vec2 c = a * b;
		CHECK( c.x == 2 );
		CHECK( c.y == 20 );

		Vec2 d = c * c;
		CHECK( d.x == 4 );
		CHECK( d.y == 400 );
	}
	SUBCASE( "Scale Multiplication" )
	{
		Vec2 c = a * scale_a;
		CHECK( c.x == 100 );
		CHECK( c.y == 500 );

		Vec2 d = a * scale_b;
		CHECK( d.x == 0.5 );
		CHECK( d.y == 2.5 );
		
	}
	SUBCASE( "Division" )
	{
		Vec2 c = a / b;
		Vec2 d = b / a;

		CHECK( abs( c.x - (1.0 / 2.0) ) < epsilon );
		CHECK( abs( c.y - (5.0 / 4.0) ) < epsilon );
		CHECK( abs( d.x - (2.0) ) < epsilon );
		CHECK( abs( d.y - (4.0 / 5.0) ) < epsilon );
	}
	SUBCASE( "Subtraction" )
	{
		Vec2 c = a - b;
		CHECK( c.x == -1 );
		CHECK( c.y == 1 );
	}
	SUBCASE( "Division" )
	{
		Vec2 c = a * scale_c;
		CHECK( c.x == 0.0 );
		CHECK( c.y == 0.0 );
	}
	SUBCASE( "Scale Division" )
	{
		Vec2 c = a / scale_a;
		CHECK( c.x == ( 1 / scale_a ) );
		CHECK( c.y == ( 5 / scale_a ) );

		Vec2 d = a / scale_b;
		CHECK( d.x == ( 1 / scale_b ) );
		CHECK( d.y == ( 5 / scale_b ) );
		
	}
}