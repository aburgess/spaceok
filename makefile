VERSION_MAJOR = 0
VERSION_MINOR = 0
VERSION_PATCH = 0

ifeq ($(OS),Windows_NT)
CXX = g++ # or your Windows compiler
CXXFLAGS = -std=c++17 -Wall -Wreorder

SDL_INCLUDE = C:/dev/SDL2/x86_64-w64-mingw32/include/SDL2
SDL_LIBRARY = C:/dev/SDL2/x86_64-w64-mingw32/lib
else
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
CXX = g++
# CXX = clang
CXXFLAGS = -std=c++17 -Wall -Wreorder

SDL_INCLUDE = 
SDL_LIBRARY = 
endif
# Add more conditions for other platforms as needed
endif

# Compiler flags

# DEBUG BUILD
CXXFLAGS += -g --coverage -O0
# CXXFLAGS += -fanalyzer
# CXXFLAGS += -Xanalyzer

# Linker Flags
LDFLAGS = 

# Source directory
SRC_DIR = src

# Include directory
INC_DIR = include

# Object files directory
BUILD_DIR = build

# Test Variables
TEST_MAIN = DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
TEST_FLAG = __TEST_RUNNER__
TEST_DEFINES = -D$(TEST_MAIN) -D$(TEST_FLAG)
TEST_TARGET = $(BIN_DIR)/test_runner
TEST_HEADER = lib/doctest
TEST_DIR = tests
TEST_BUILD_DIR = $(BUILD_DIR)/.test

# Executable directory
BIN_DIR = bin

# SDL Inclusions
SDL_INCLUDE = C:/dev/SDL2/x86_64-w64-mingw32/include/SDL2
SDL_LIBRARY = C:/dev/SDL2/x86_64-w64-mingw32/lib
SDL_LINKER_OPTIONS = -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
# SDL_OPTIONS = -lmingw32 -lSDL2main -lSDL2

LDFLAGS += $(SDL_LINKER_OPTIONS)

# Source files
SRC_FILES = $(wildcard $(SRC_DIR)/**/*.cpp $(SRC_DIR)/*.cpp )

# Test Files
TEST_FILES = $(wildcard $(TEST_DIR)/**/*.test.cpp $(TEST_DIR)/*.test.cpp)

# Object files
OBJ_FILES = $(patsubst $(SRC_DIR)/%.cpp,$(BUILD_DIR)/%.o,$(SRC_FILES))
TEST_OBJ_FILES = $(patsubst $(TEST_DIR)/%.test.hpp,$(TEST_BUILD_DIR)/%.test.o,$(TEST_FILES))
TEST_SRC_OBJ_FILES = $(patsubst $(SRC_DIR)/%.cpp,$(TEST_BUILD_DIR)/%.src.o,$(SRC_FILES))
# TEST_SRC_OBJ_FILES = $(patsubst $(SRC_DIR)/%.cpp,$(TEST_BUILD_DIR)/%.src.o,$(SRC_FILES))

# Output executable
TARGET = $(BIN_DIR)/game

# all: $(TARGET) $(TEST_TARGET)
all: $(TARGET) test

test: $(TEST_TARGET)
	./$(TEST_TARGET)

game: $(TARGET)

$(TARGET): $(OBJ_FILES)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJ_FILES) -L$(SDL_LIBRARY) $(LDFLAGS)

$(TEST_TARGET): $(TEST_OBJ_FILES) $(TEST_SRC_OBJ_FILES)
	$(CXX) $(CXXFLAGS) -o $(TEST_TARGET) $(TEST_OBJ_FILES) $(TEST_SRC_OBJ_FILES) -L$(SDL_LIBRARY) $(SDL_OPTIONS) $(LDFLAGS)

$(TEST_BUILD_DIR)/%.src.o: $(SRC_DIR)/%.cpp ${INC_DIR}/%.h
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(TEST_DEFINES) $(LDFLAGS) -I$(SDL_INCLUDE) -I$(INC_DIR) -I$(TEST_HEADER) -I$(TEST_DIR) -c -o $@ $<

$(TEST_BUILD_DIR)/%.test.o: $(SRC_DIR)/%.test.hpp $(SRC_DIR)/%.cpp ${INC_DIR}/%.h
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(TEST_DEFINES) $(LDFLAGS) -I$(SDL_INCLUDE) -I$(INC_DIR) -I$(TEST_HEADER) -c -o $@ $<

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp ${INC_DIR}/%.h
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -I${SDL_INCLUDE} -I$(INC_DIR) -c -o $@ $<

clean:
	rm -rf $(TEST_BUILD_DIR)/* $(TEST_TARGET)
	rm -rf $(BIN_DIR)/* $(BUILD_DIR)/*
	rm -rf ./*~ ./*.swp ./*.gcno
	rm -rf *~

remake: clean $(TARGET)

run: $(TARGET)
	./$(TARGET)
